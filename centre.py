from data_converter.output_object import OutputObject

class Centre(OutputObject):
    def __init__(self):
        self.codi = ""
        self.nom = ""
        self.st = 0
        self.codi_zona1 = ""
        self.codi_zona2 = ""
        self.codi_municipi = ""
        self.cmc = False
        self.codi_comarca = ""
        self.districte_municipal = 0,
        self.adreca = ""
        self.codi_postal = ""
        self.telefon = ""
        self.email = ""
        
    def __str__(self):
        return "[codi: " + self.codi + \
               ", nom: " + self.nom + \
               "]"