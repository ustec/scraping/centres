CREATE TABLE IF NOT EXISTS `centres2425` (
  `codi` varchar(8) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `st` int(11) NOT NULL,
  `codi_municipi` varchar(5),  
  `codi_zona1` varchar(6),
  `codi_zona2` varchar(6),
  `codi_comarca` varchar(2),
  `districte_municipal` tinyint,
  `adreca` varchar(255),
  `codi_postal` varchar(5),
  `telefon` varchar(15),
  `email` varchar(50),
  `cmc` boolean,
  PRIMARY KEY (`codi`),
  FOREIGN KEY (`codi_municipi`) REFERENCES `municipi`(`codi`),
  FOREIGN KEY (`codi_zona1`) REFERENCES `zona`(`codi`),
  FOREIGN KEY (`codi_zona2`) REFERENCES `zona`(`codi`),
  FOREIGN KEY (`codi_comarca`) REFERENCES `comarca`(`codi`),
  FOREIGN KEY (`districte_municipal`) REFERENCES `districte_municipal`(`codi`)
);

CREATE TABLE IF NOT EXISTS `municipi` (
  `codi` varchar(6) NOT NULL,
  `nom` varchar(100) NOT NULL,
  PRIMARY KEY (`codi`)
);
CREATE TABLE IF NOT EXISTS `comarca` (
  `codi` varchar(2) NOT NULL,
  `nom` varchar(100) NOT NULL,
  PRIMARY KEY (`codi`)
);
CREATE TABLE IF NOT EXISTS `districte_municipal` (
  `codi` tinyint NOT NULL,
  `nom` varchar(100) NOT NULL,
  PRIMARY KEY (`codi`)
);