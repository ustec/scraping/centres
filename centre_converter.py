from data_converter.data_converter import DataConverter
from config import ConfigCentre
from centre import Centre
import csv
import logging
import requests
import os
import re
from sqlalchemy.orm import mapper
from sh import pdftotext
from sh import iconv
from shutil import copyfile

class CentreConverter(DataConverter):
            
    def __init__(self, config_file_path):
        self.config = ConfigCentre(config_file_path)
        super(CentreConverter, self).__init__()
        mapper(Centre, self.table) 
        logging.basicConfig(filename=self.config.log_file, level=logging.WARNING)
    
    def get_input_files(self):
        """Get PDFs with all nomenaments of dificil cobertura (except Consorci)"""
        local_filenames = ["plantilles.pdf", "adjudicacions.pdf", "centres.csv"]
        # Loop all URLS downloading documents
        urls = open(self.config.urls).readlines()
        i = 0
        for u in urls:
            print(u)
            remote_file = u.strip()
            local_file = os.path.join(self.config.raw_data_dir, local_filenames[i])
            response = requests.get(remote_file)
            with open(local_file, 'wb') as f:
                f.write(response.content)
            i = i + 1
        
    def convert(self):
        """Convert PDFs (adjudicacions d'estiu and plantilles DOGCs) to text.
        Centres CSV is kept as is, CSV is readable enough."""
        # Convert PDFs to text
        pdftotext('-nopgbrk', 
                  os.path.join(self.config.raw_data_dir, 'plantilles.pdf'), 
                  os.path.join(self.config.data_dir, "plantilles.txt"))
        pdftotext('-nopgbrk', 
                  os.path.join(self.config.raw_data_dir, 'adjudicacions.pdf'), 
                  os.path.join(self.config.data_dir, "adjudicacions.txt"))
        # Copy csv as-is
        iconv('-f', 'ISO8859-15', 
                  '-t', 'UTF-8',
                 os.path.join(self.config.raw_data_dir, "centres.csv"), 
                 '-o', os.path.join(self.config.data_dir, "centres.csv"))
         
    def get_valid_records(self):
        """Get all centres of adjudicacions estiu and plantilles DOGCS.""" 
        records= []
        centre_codes = set()
        # Get lines with interesting data: zones, municipis and centres
        files = sorted(os.listdir(self.config.data_dir))
        for filename in files:
            print(filename)
            # Only look for plantilles and ajudicacions
            if filename.endswith(".txt"):
                with open(os.path.join(self.config.data_dir,filename), "r") as r:
                    st = 0
                    zona_code = ""
                    municipi_code = ""
                    for l in r:
                        l = l.strip()
                        centre = None
                        # Get servei territorial number
                        st = self.get_st(l, st)
                        # Get current zona code and name
                        zona_code= self.get_zona(l, zona_code)
                        # Get current municipi code and name
                        municipi_code = self.get_municipi(l, municipi_code)
                        # Get current centre code and name
                        centre = self.get_centre(l)
                        # If current line is a centre, fill record with centre data
                        if centre is not None and centre[0] not in centre_codes:
                            isCMC = self.checkCMC(centre[1])
                            record = []
                            record.append(centre[0])
                            record.append(centre[1].replace("(CMC)", ""))
                            record.append(st)
                            zones = []
                            zones.append(zona_code)
                            record.append(zones)
                            record.append(municipi_code)
                            record.append(isCMC)
                            # Look for more centre data in csv
                            record = self.add_more_data_from_csv(record)
                            records.append(record)
                            centre_codes.add(centre[0])
                        elif centre is not None and centre[0] in centre_codes and zona_code != '':
                            for r in records:
                                if r[0] == centre[0]:                                  
                                    r[3].append(zona_code)
        return records
    
    @staticmethod
    def get_st(l, prev_st):
        """Gets servei territorial code if line is a servei territorial name. 
        Returns current servei territorial otherwise."""
        st = prev_st
        if l.strip() == "Consorci d'Educació de Barcelona":
            st = 1
        elif l.strip() == "Serveis Territorials a Barcelona Comarques":
            st = 2
        elif l.strip() == "Serveis Territorials al Baix Llobregat":
            st = 3
        elif l.strip() == "Serveis Territorials al Vallès Occidental":
            st = 4
        elif l.strip() == "Serveis Territorials al Maresme-Vallès Oriental":
            st = 5
        elif l.strip() == "Serveis Territorials a la Catalunya Central":
            st = 6
        elif l.strip() == "Serveis Territorials a Girona":
            st = 17
        elif l.strip() == "Serveis Territorials a Lleida":
            st = 25
        elif l.strip() == "Serveis Territorials a Tarragona":
            st = 43
        elif l.strip() == "Serveis Territorials a les Terres de l'Ebre":
            st = 44
        elif l.strip() == "Serveis Territorials a l'Alt Pirineu i Aran":
            st = 25
        elif l.strip() == "Serveis Territorials al Penedès":
            st = 7
        return st
    
    @staticmethod
    def get_zona(l, prev_zona):
        """Gets zona code and name if line is a zona. 
        Returns current zona otherwise."""
        zona_code = prev_zona
        # zona codes have 6 digits
        if re.match(r"Zona: [0-9]{6}", l):
            # Line format is "Zona: 083549 Igualada."
            # Get code
            zona_code = l.split(" ", 2)[1]
        return zona_code
    
    @staticmethod
    def get_municipi(l, prev_municipi):
        """Gets municipi code and name if line is a municipi. 
        Returns current municipi otherwise."""
        municipi_code = prev_municipi
        # municipi codes have 5 digits
        if re.match(r"Municipi: [0-9]{5}", l):
            # Line format is "Municipi: 08175 - Puig-reig"
            # Get code
            municipi_code = l.split(" ", 2)[1]
        return municipi_code
    
    @staticmethod
    def get_centre(l):
        """Gets centre code and name if line is a centre. 
        Returns None otherwise."""
        # municipi codes have 8 digits
        if re.match(r"[0-9]{8}", l):
            # Remove extra text codes and get centre code and name
            # Do not remove CMC, because it is needed after
            return l.strip().replace(":", "") \
                                  .replace("(1)", "") \
                                  .replace("(2)", "") \
                                  .replace("(3)", "") \
                                  .replace("(CRFE)", "") \
                                  .replace("(CRFP)", "") \
                                  .replace("(CRF)", "") \
                                  .replace("(A)", "") \
                                  .replace("(EDIF)", "") \
                                  .replace("(CFACP)", "") \
                                  .split(" ", 1)
        else:
            return None
     
    @staticmethod   
    def checkCMC(centre):
        """ Check if centre is a CMC one """
        if "(CMC)" in  centre:
            return True
        return False
 
    def add_more_data_from_csv(self, r):
        """For each record gets more data of CSV file if centre has info in this file"""
        centres_csv = csv.reader(open(os.path.join(self.config.data_dir, "centres.csv"), "r"), delimiter=";")
        # Field for comarca code
        r.append("")
        # Field for address
        r.append("")
        # Field for postal code
        r.append("")
        # Field for phone
        r.append("")
        # Field for mail
        r.append("")
        # Field for districte municipal (only Barcelona)
        r.append("")
        for row_csv in centres_csv:
            # If centre is in CSV (centre codes are the same)
            if r[0] == row_csv[0]:
                if r[4] == '':
                    # Get municipi code
                    r[4] = row_csv[13]
                # Get comarca code
                r[6] = row_csv[11]
                # Get address
                r[7] = row_csv[6]
                # Get postal code
                r[8] = row_csv[7]
                # Get phone
                r[9] = row_csv[8]
                # Get mail
                r[10] = row_csv[23]
                # Get districte municipal (only Barcelona)
                r[11] = row_csv[15]
                break
        return r
        
    def build_objects(self, raw_records):
        """Build objects from records to be saved in db"""
        for r in raw_records:
            centre = Centre()
            centre.codi = r[0].strip()
            centre.nom = r[1].strip()
            centre.st = r[2]
            centre.codi_zona1 = r[3][0].strip() or None
            if len(r[3]) > 1:
                centre.codi_zona2 = r[3][1].strip()
            else:
                centre.codi_zona2 = None
            centre.codi_municipi = r[4].strip() or None
            centre.cmc = r[5]
            centre.codi_comarca = r[6].strip() or None
            centre.adreca = r[7].strip() or None
            centre.codi_postal = r[8].strip() or None
            centre.telefon = r[9].strip() or None
            centre.email = r[10].strip() or None
            centre.districte_municipal = r[11].strip() or None
            self.objects.append(centre)